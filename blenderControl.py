import os
import bpy          
import sys
import time
import shutil
import argparse
import math
import mathutils

from pathlib import Path    # For generating folder, remove if not needed.

# Importing custom modules
# This first bit is necessary for importing custom python modules from inside Blender.
# It points Python to the location of the Blender file.
sys.path.append(os.path.dirname(bpy.data.filepath))
from modules import setup_functions, rendering_functions

#----------------------------------------------------------------------------------#
# DATAFILES # Everything about spectra can be removed not, right?

# S-type asteroid spectra
#asteroidSpectra = "data/S-type-asteroid-spectra.txt"

# Comet 67p spectra 
#churyumovSpectra = "data/67P-spectra.txt"

#----------------------------------------------------------------------------------#

# ARGUMENT PARSING

argv = sys.argv

if "--" not in argv:
    argv = []  # as if no args are passed
else:
    # Get all args after "--" and saves them in a list
    argv = argv[argv.index("--") + 1:]

parser = argparse.ArgumentParser()

# Argument extraction.

## Datafiles

parser.add_argument("--fromFile", dest = "fromFile", action = "store_true", required = False,
        help = "Reads positional data from external file")

parser.add_argument("--positionalData", dest = "positionalData", nargs = 1, required = False,
        help = "Location of the positional data for camera and Sun")

# Needed?
parser.add_argument("--spectralData", dest = "spectralData", nargs = 1, required = False,
        help = "Location of the spectral data file")

parser.add_argument("--coordinates", dest = "coordinates", nargs = 2, required = False,
        help = "Reads coordinates of sun and camera") 

# Needed?
parser.add_argument("--calibration", dest = "calibration", action = "store_true",
        help = "Asks for calibration images with a white Lambertian disk")


## Material

parser.add_argument("--surfaceFunctionName", dest = "surfaceFunctionName", nargs = 1, required = True,
        help = "Name of the photometric function for the surface")

parser.add_argument("--boulderFunctionName", dest = "boulderFunctionName", nargs = 1, required = True,
        help = "Name of the photometric function for the boulders")

# These should be required only if ROLO is selected.
## Although blender will fail anyway if you select ROLO/Mc but don't provide the values.

parser.add_argument("--ROLOG0", dest = "ROLOG0", nargs = 1, required = False,
        help = "ROLO parameter G0")
parser.add_argument("--ROLOG1", dest = "ROLOG1", nargs = 1, required = False,
        help = "ROLO parameter G1")
parser.add_argument("--ROLOA0", dest = "ROLOA0", nargs = 1, required = False,
        help = "ROLO parameter A0")
parser.add_argument("--ROLOA1", dest = "ROLOA1", nargs = 1, required = False,
        help = "ROLO parameter A1")
parser.add_argument("--ROLOA2", dest = "ROLOA2", nargs = 1, required = False,
        help = "ROLO parameter A2")
parser.add_argument("--ROLOA3", dest = "ROLOA3", nargs = 1, required = False,
        help = "ROLO parameter A3")
parser.add_argument("--ROLOA4", dest = "ROLOA4", nargs = 1, required = False,
        help = "ROLO parameter A4")

parser.add_argument("--McEEpsilon", dest = "McEEpsilon", nargs = 1, required = False,
        help = "McEwen parameter epsilon")
parser.add_argument("--McEKsi", dest = "McEKsi", nargs = 1, required = False,
        help = "McEwen parameter ksi")

parser.add_argument("--McEEta", dest = "McEEta", nargs = 1, required = False,
        help = "McEwen parameter eta")

parser.add_argument("--McEBeta", dest = "McEBeta", nargs = 1, required = False,
        help = "McEwen parameter beta")
parser.add_argument("--McEGamma", dest = "McEGamma", nargs = 1, required = False,
        help = "McEwen parameter gamma")
parser.add_argument("--McEDelta", dest = "McEDelta", nargs = 1, required = False,
        help = "McEwen parameter delta")

## Target 

parser.add_argument("--target", dest = "target", nargs = 1, required = True,
        help = "Choice of target geometry")

parser.add_argument("--proceduralDetail", dest = "proceduralDetail", nargs = 1, required = True,
        help = "State of the procedural detail modifier")

# NEW!
parser.add_argument("--satellite", dest = "satellite", nargs = 1, required = False,
        help = "Presence of satellite, if available")

parser.add_argument("--systemRotation", dest = "systemRotation", nargs = 1, required = True,
        help = "Rotation of the system, in degrees")

parser.add_argument("--resolution", dest = "resolution", nargs = 2, required = True,
        help = "Resolution of the final renders")

parser.add_argument("--fov", dest = "fov", nargs = 2, required = True,
        help = "Field of view size in degrees")

parser.add_argument("--outDir", dest = "outDir", nargs = 1, required = False,
        help = "Output directory for renders, overwites default scheme with OBJ-FUNC-FUNC-DATE-TIME.")

parser.add_argument("--angularCoordinates", dest = "angularCoordinates", action = "store_true",
        required = False,
        help = "Whether or not angular coordinates are in use")

parser.add_argument("--systemScale", dest = "systemScale", nargs = 1, required = True,
        help = "Scale of the system, unitless")


## Albedo

parser.add_argument("--imageAlbedo", dest = "imageAlbedo", nargs = 1, required = True,
        help = "Image albedo value.")

parser.add_argument("--albedoVariations", dest = "albedoVariations", action = "store_true", 
        required = False, help = "Albedo variations selected.")

parser.add_argument("--albedoRange", dest = "albedoRange", nargs = 1, required = False, 
        help = "Percentage of albedo variation with respect to imageAlbedo.")

parser.add_argument("--noiseScale", dest = "noiseScale", nargs = 1, required = False, 
        help = "Scale of the procedural noise texture, used for albedo variation..")

parser.add_argument("--noiseSeed", dest = "noiseSeed", nargs = 1, required = False, 
        help = "Random seed for the procedural noise texture.")

parser.add_argument("--noiseDetail", dest = "noiseDetail", nargs = 1, required = False, 
        help = "Detail of the procedural noise texture.")

parser.add_argument("--noiseRoughness", dest = "noiseRoughness", nargs = 1, required = False, 
        help = "Roughness of the procedural noise texture.")

parser.add_argument("--noiseWhiteLevel", dest = "noiseWhiteLevel", nargs = 1, required = False, 
        help = "White level of the procedural noise texture.")

parser.add_argument("--noiseBlackLevel", dest = "noiseBlackLevel", nargs = 1, required = False, 
        help = "Black level of the procedural noise texture.")

args = parser.parse_args(argv)
print(args)	


#----------------------------------------------------------------------------------#

# Scene setup

scene = bpy.context.scene

targetObject = bpy.context.scene.objects[args.target[0]]

## Move to frame 1, just in case.
bpy.context.scene.frame_set(1)

## Target setup

#setup_functions.targetSetup(args.target[0])

try:

    if args.satellite[0] == "True":
        setup_functions.targetSetup(args.target[0])

    elif args.satellite[0] == "False":
        setup_functions.targetSetup(args.target[0], satellite = False)

except:
    # For compatibility with main.py, which doesn't have this option.
    # But maybe it should!

    setup_functions.targetSetup(args.target[0])

## Procedural detail

# Translate the string to booleans
if args.proceduralDetail[0] == "True":
    state = True
else:
    state = False

# Try/Except, in case the object doesn't have a geometry nodes modifier
try:
    setup_functions.proceduralDetail(args.target[0], state)
except:
    pass

## Material setup

surfaceMaterialName = args.surfaceFunctionName[0]
boulderMaterialName = args.boulderFunctionName[0]

# I could create duplicates of the materials, called material_boulder.
# Then boulderMaterialName = blablabla + "_boulder"

# Or adding it down here, I think it's easier. boulderMaterialName doesn't change, so the ifs keep working,
# but the boulderMaterial object is actually a boulder material.

surfaceMaterial = bpy.data.materials[surfaceMaterialName]
#boulderMaterial = bpy.data.materials[boulderMaterialName]

# NEW!
boulderMaterial = bpy.data.materials[boulderMaterialName + "_boulders"]

# Image brightness is controlled via sun lamp power. Parameters
# for Lambert and Lommel-Seeliger (diffuse and volumetric) shaders
# are always k = 0.0625 and w = 0.5, making the normal albedo of both as 0.125


# Additional parameters for photometric functions
if surfaceMaterialName == "ROLO":
    surfaceMaterial.node_tree.nodes["G0"].outputs[0].default_value = float(args.ROLOG0[0])
    surfaceMaterial.node_tree.nodes["G1"].outputs[0].default_value = float(args.ROLOG1[0])
    surfaceMaterial.node_tree.nodes["A0"].outputs[0].default_value = float(args.ROLOA0[0])
    surfaceMaterial.node_tree.nodes["A1"].outputs[0].default_value = float(args.ROLOA1[0])
    surfaceMaterial.node_tree.nodes["A2"].outputs[0].default_value = float(args.ROLOA2[0])
    surfaceMaterial.node_tree.nodes["A3"].outputs[0].default_value = float(args.ROLOA3[0])
    surfaceMaterial.node_tree.nodes["A4"].outputs[0].default_value = float(args.ROLOA4[0])

elif surfaceMaterialName == "McEwen":
    surfaceMaterial.node_tree.nodes["epsilon"].outputs[0].default_value = float(args.McEEpsilon[0])
    surfaceMaterial.node_tree.nodes["ksi"].outputs[0].default_value = float(args.McEKsi[0])
    surfaceMaterial.node_tree.nodes["eta"].outputs[0].default_value = float(args.McEEta[0])
    surfaceMaterial.node_tree.nodes["beta"].outputs[0].default_value = float(args.McEBeta[0])
    surfaceMaterial.node_tree.nodes["gamma"].outputs[0].default_value = float(args.McEGamma[0])
    surfaceMaterial.node_tree.nodes["delta"].outputs[0].default_value = float(args.McEDelta[0])

if boulderMaterialName == "ROLO":
    boulderMaterial.node_tree.nodes["G0"].outputs[0].default_value = float(args.ROLOG0[0])
    boulderMaterial.node_tree.nodes["G1"].outputs[0].default_value = float(args.ROLOG1[0])
    boulderMaterial.node_tree.nodes["A0"].outputs[0].default_value = float(args.ROLOA0[0])
    boulderMaterial.node_tree.nodes["A1"].outputs[0].default_value = float(args.ROLOA1[0])
    boulderMaterial.node_tree.nodes["A2"].outputs[0].default_value = float(args.ROLOA2[0])
    boulderMaterial.node_tree.nodes["A3"].outputs[0].default_value = float(args.ROLOA3[0])
    boulderMaterial.node_tree.nodes["A4"].outputs[0].default_value = float(args.ROLOA4[0])

elif boulderMaterialName == "McEwen":
    boulderMaterial.node_tree.nodes["epsilon"].outputs[0].default_value = float(args.McEEpsilon[0])
    boulderMaterial.node_tree.nodes["ksi"].outputs[0].default_value = float(args.McEKsi[0])
    boulderMaterial.node_tree.nodes["eta"].outputs[0].default_value = float(args.McEEta[0])
    boulderMaterial.node_tree.nodes["beta"].outputs[0].default_value = float(args.McEBeta[0])
    boulderMaterial.node_tree.nodes["gamma"].outputs[0].default_value = float(args.McEGamma[0])
    boulderMaterial.node_tree.nodes["delta"].outputs[0].default_value = float(args.McEDelta[0])

# Assign the materials to the object using geometry nodes.

geoNodesParent = bpy.data.objects[args.target[0]].modifiers["GeometryNodes"]

# Input_4: Surface material. Would be great if to find by name.
geoNodesParent["Input_4"] = surfaceMaterial

# Input_5: Boulder material
geoNodesParent["Input_5"] = boulderMaterial  


# Same thing for the children object, if any:
# What if there was more than one satellite?
if targetObject.children:
    geoNodesChildren = targetObject.children[0].modifiers["GeometryNodes"]
    geoNodesChildren["Input_4"] = surfaceMaterial
    geoNodesChildren["Input_5"] = boulderMaterial  


# Albedo variations

# NEW! I think changes are required here, for the boulder's albedo.

if (surfaceMaterialName == "Lambert" or 
    surfaceMaterialName == "Lommel-Seeliger" or
    surfaceMaterialName == "ROLO" or
    surfaceMaterialName == "McEwen"):   # Add any other material with albedo variations here.

    albedoVariationNode = surfaceMaterial.node_tree.nodes["Albedo variation"]

    if not args.albedoVariations:
        # No albedo variations selected by the user.

        albedoVariationNode.outputs[0].default_value = 0

    else:

        albedoVariationNode.outputs[0].default_value = float(args.albedoRange[0]) * 0.01  

        # Noise node

        noiseNode = surfaceMaterial.node_tree.nodes["Noise Texture"] 

        seedValue       =   noiseNode.inputs[1]
        scaleValue      =   noiseNode.inputs[2]
        detailValue     =   noiseNode.inputs[3]
        roughnessValue  =   noiseNode.inputs[4]

        seedValue.default_value         =   float(args.noiseSeed[0])
        scaleValue.default_value        =   float(args.noiseScale[0])
        detailValue.default_value       =   float(args.noiseDetail[0])
        roughnessValue.default_value    =   float(args.noiseRoughness[0])

        # ColorRamp node

        colorRampNode = surfaceMaterial.node_tree.nodes["ColorRamp"]

        blackLevel  =   colorRampNode.color_ramp.elements[0] 
        whiteLevel  =   colorRampNode.color_ramp.elements[1] 

        blackLevel.position     =   float(args.noiseBlackLevel[0]) 
        whiteLevel.position     =   float(args.noiseWhiteLevel[0])
        

# Same for boulder material

if (boulderMaterialName == "Lambert" or 
    boulderMaterialName == "Lommel-Seeliger" or
    boulderMaterialName == "ROLO" or
    boulderMaterialName == "McEwen"):   # Add any other material with albedo variations here.

    albedoVariationNode = boulderMaterial.node_tree.nodes["Albedo variation"]

    if not args.albedoVariations:
        # No albedo variations selected by the user.

        albedoVariationNode.outputs[0].default_value = 0

    else:

        albedoVariationNode.outputs[0].default_value = float(args.albedoRange[0]) * 0.01  

        # And the rest (noise texture and colorramp are not needed.
        # Each boulder has a random value between zero and one.

###############################

## Scale
scale = float(args.systemScale[0])
bpy.context.scene.objects[args.target[0]].scale = (scale, scale, scale)

## Sun and camera

camera = bpy.data.objects["Main Camera"]
sun = bpy.data.objects["Sun"]

## Camera angle
# This works, but I'd prefer to find the camera by name

bpy.data.cameras[0].angle_x = float(args.fov[0])
bpy.data.cameras[0].angle_y = float(args.fov[1])


## Coordinates, illumination and rotation.

# Sun irradiance. With shader parameters k=0.125 and w=1,
# sun power of 8*pi makes surface brightness at backscattering
# (normal albedo) in images to RGB=(1,1,1). Therefore, scaling
# 8*pi with imageAlbedo (ia) results surface normal albedo in
# images to RGB=(ia, ia, ia).
#imageAlbedo = float(args.imageAlbedo[0])

#sunIrradiance = 8 * math.pi * imageAlbedo
# Changing the multiplier to 16 bc now k = 0.0625 and w= .5
#sunIrradiance = 16 * math.pi * imageAlbedo
sunIrradiance = 16 * math.pi * float(args.imageAlbedo[0])
    
setup_functions.sunIrradiance(sun, sunIrradiance)


if not args.fromFile:
    """
    Only one image will be rendered.
    This reads and sets the default coordinates found in settings.py
    """

    # Sets the length of the animation to one frame.
    # (This is simpler than writing specific code for rendering one image.)
    scene.frame_end = 1

    # Sun coordinates

    sunCoordinates = [float(i.strip()) for i in args.coordinates[0][1:-1].split(",")]

    if args.angularCoordinates:
        (sun_lat, sun_lon) = sunCoordinates
        setup_functions.sunSetupSpherical(sun, sun_lat, sun_lon)
    else:
        (sun_x, sun_y, sun_z) = sunCoordinates
        setup_functions.sunSetupCartesian(sun, sun_x, sun_y, sun_z)


    #setup_functions.sunIrradiance(sun, float(args.sunIrradiance[0]))
    #setup_functions.sunIrradiance(sun, sunIrradiance)

    # Camera coordinates

    cameraCoordinates = [float(i.strip()) for i in args.coordinates[1][1:-1].split(",")]

    if args.angularCoordinates:
        (camera_lat, camera_lon, camera_dist) = cameraCoordinates 
        setup_functions.cameraSetupSpherical(camera, camera_lat, camera_lon, camera_dist)
    else:
        (camera_x, camera_y, camera_z) = cameraCoordinates
        setup_functions.cameraSetupCartesian(camera, camera_x, camera_y, camera_z)

    # Target rotation

    #setup_functions.systemRotation(args.target[0], int(args.systemRotation[0]))
    setup_functions.systemRotation(args.target[0], float(args.systemRotation[0]))

else:
    """
    Loops through the items in the file and adds animation keyframes,
    one per line.
    """
        
    filepath = os.path.join(os.path.dirname(bpy.data.filepath), args.positionalData[0])

    # File reading.

    with open(filepath) as f:
        lines = f.readlines()


    # Frame setup.
    
    startFrame = 1
    endFrame = len(lines[1:])
    scene.frame_end = endFrame # Sets the end frame to the size of the dataset

    # Looping through the file.

    frame = 1

    for line in lines[1:]:      # [1:] skips the first line (that contains column names)

        # Move the scene to the appropriate frame
        scene.frame_set(frame)

        # Split the data string on its whitespaces
        line = line.split()

        # Extract calues from the datafile

        if args.angularCoordinates:


            camera_lat  =   float(line[0])
            camera_lon  =   float(line[1])
            camera_dist =   float(line[2])

            sun_lat     =   float(line[3])
            sun_lon     =   float(line[4])

            rot         =   float(line[5])

        else:

            camera_x    =   float(line[0])
            camera_y    =   float(line[1])
            camera_z    =   float(line[2])

            sun_x       =   float(line[3])
            sun_y       =   float(line[4])
            sun_z       =   float(line[5])

            rot         =   float(line[6])

    
        # Values are applied, and new Blender keyframes are created.

        ## Sun setup

        if args.angularCoordinates:
            setup_functions.sunSetupSpherical(sun, sun_lat, sun_lon)

        else:
            setup_functions.sunSetupCartesian(sun, sun_x, sun_y, sun_z)


        # PROVISIONAL!!
        #sun_irr = 10
        #setup_functions.sunIrradiance(sun, sun_irr)
        #setup_functions.sunIrradiance(sun, args.sunIrradiance[0])
        #setup_functions.sunIrradiance(sun, sunIrradiance)

        sun.keyframe_insert(data_path = "location", frame = frame)
        sun.keyframe_insert(data_path = "rotation_euler", frame = frame)
        sun.data.keyframe_insert(data_path = "energy", frame = frame)

        ## Camera setup
        
        if args.angularCoordinates:
            setup_functions.cameraSetupSpherical(camera, camera_lat, camera_lon, camera_dist)

        else:
            setup_functions.cameraSetupCartesian(camera, camera_x, camera_y, camera_z)

        camera.keyframe_insert(data_path = "location", frame = frame)
        camera.keyframe_insert(data_path = "rotation_euler", frame = frame)

        ## System rotation setup

        setup_functions.systemRotation(args.target[0], rot)
        #targetObject = bpy.context.scene.objects[args.target[0]]

        targetObject.keyframe_insert(data_path = "rotation_euler", frame = frame)

        if targetObject.children:

            childObject = targetObject.children[0]
            childObject.keyframe_insert(data_path = "rotation_euler", frame = frame)


        # And finally advance one frame
        frame += 1

# Re-set the camera as the render camera, just in case.
scene.camera = camera

# Move to frame 1 again.
bpy.context.scene.frame_set(1)

#----------------------------------------------------------------------------------#

# RENDERING

resolutionX = int(args.resolution[0])
resolutionY = int(args.resolution[1])

if args.outDir:

  #foldername = args.outDir[0] 
  foldername = f"Renders/{args.outDir[0]}/Original"

else:
    # Renders all the (animation) frames in a new folder inside /Renders.

    #foldername = (f"//Renders/{args.target[0]}-"
    foldername = (f"Renders/{args.target[0]}-"
        f"{args.surfaceFunctionName[0]}-{args.boulderFunctionName[0]}-"
        f"{time.strftime('%d%m%Y-%H%M%S')}/Original")

#setup_functions.sunIrradiance(sun, 250) # For Antti's stuff!
#setup_functions.sunIrradiance(sun, 150)
#setup_functions.sunIrradiance(sun, float(args.sunIrradiance[0])) 

"""
if args.outDir:
    filename = str(args.wavelength[0]) + "nm" + "-si" + str(sunIntensity) + "-sun" + str(sun_x) + "_" + str(sun_y) + "_" + str(sun_z) + "-camera" + str(camera_x) + "_" + str(camera_y) + "_" + str(camera_z)

else:
    filename = "frame"      # find a better name!
"""

filename = "frame"

# I think I should create the folder here and pass it to the rendering function,
# but I would need to make sure that the make folder thing works on windows as well.
rendering_functions.renderAnimation(scene, resolutionX, resolutionY, foldername, filename)

if args.calibration:

    if args.fromFile:

        rendering_functions.renderCalibrationDisks(scene, resolutionX, resolutionY,
                foldername, filename, filepath)

    else:

        rendering_functions.renderCalibrationDisks(scene, resolutionX, resolutionY,
                foldername, filename)

# AP: if-else not needed
#if not args.outDir:

import json
# Here I save the args used to a json file for future reference.
with open(foldername + "/settings-copy.json", "w") as file:
    #file.write(str(args))
    json.dump(args.__dict__, file, indent=2)

    # And here I backup the parameters file that was used to the folder.
if args.fromFile:
    shutil.copyfile(args.positionalData[0], foldername + "/data-copy.txt")

#else:
#
#    with open(foldername + "/settings.txt", "w") as file:
#        file.write(str(args))
#
#    if args.fromFile:
#        shutil.copyfile(args.positionalData[0], foldername + "/data.txt")

print("Script finished.")
