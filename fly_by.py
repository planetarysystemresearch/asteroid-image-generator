#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 14 15:08:44 2021

@author: arondahl
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


##FOR USER TO DEFINE: 

##-Satellites initial coordinates given in kilometers.
sat_ini_vec=np.array([10000,10000,10000])


##Sun's rotation parameters around x,y,z axes, given in degrees.
sun_rot_x,sun_rot_y,sun_rot_z=35,150,0


##Closest approach distance given in kilometers.
d=3000


##How many images are wanted, the number of images is 2*n
n=100


##Other things user might want to change, if necessary:

##Sun Irradiance and rotation of the asteroid, used in Mario's code.

sun_irr=250

rot=60





##HERE BEGIN'S THE CODE, WHICH USER DOES NOT HAVE TO CHANGE.



##These are used in calculating one possible vector for the closest approach location. 
##The vector is then rotated to match user's desire (most illuminated side, mid-way or least-illuminated side)
a=sat_ini_vec[0]
b=sat_ini_vec[1]
c=sat_ini_vec[2]



##Normalizing satellite's initial vector for calculation purposes.
sat_ini_vec_length=np.sqrt((sat_ini_vec[0])**2 + (sat_ini_vec[1])**2 + (sat_ini_vec[2])**2)
unit_sat_ini_vec=sat_ini_vec/sat_ini_vec_length


##This function calculates the suns vector. That vector is used for taking the dot product with closest-approach vector in order to calculate
##the most-illuminated side, for example.

def sunRotationToCoords(sun_rot_x,sun_rot_y,sun_rot_z): 
    sun_rot_x=np.deg2rad(sun_rot_x)
    sun_rot_y=np.deg2rad(sun_rot_y)
    sun_rot_z=np.deg2rad(sun_rot_z)
    
    sun_x=-np.sin(sun_rot_y)*np.cos(sun_rot_z)
    sun_y=np.sin(sun_rot_x)*np.cos(sun_rot_z)
    sun_z=-np.cos(sun_rot_x)*np.cos(sun_rot_y)
    return np.array([sun_x, sun_y, sun_z])


##Making of the vector from the coordinate points returned by the above function. Also normaizing it for calculation purposes.
sun_vec = sunRotationToCoords(sun_rot_x,sun_rot_y,sun_rot_z)

sun_vec_length=np.sqrt((sun_vec[0])**2 + (sun_vec[1])**2 + (sun_vec[2])**2)

unit_sun_vec=sun_vec/sun_vec_length




    
##Calculating one possible closest approach vector, p. Derivation of these equations can be found in the wiki.
if (b!=0):
    p_x=np.sqrt(d**2/(1+a**2/b**2))

    p_y=-(a/b)*p_x

else:
    p_x=0
    p_y=d


p_z=0

p=np.array([p_x,p_y,p_z])



##Vector cross-product of unit-size satellite's initial vector and closest approach vector p. Vector w is used in the rotation process.
w=-np.cross(unit_sat_ini_vec,p)


##Taking evenly spaced values of rotation angle between [0, 2pi] 
theta=np.linspace(0,2*np.pi,100)



##Rotating the vector and calculating the dot-product of the rotated vector and sun's vector to define the illuminated side.
p_rotated=np.zeros((100,3))
illumination=np.zeros((100,1))
for i in range(len(theta)):
    p_rotated[i]+=p*np.cos(theta[i]) + w*np.sin(theta[i])
    illumination[i]+=np.dot(unit_sun_vec,p_rotated[i])





##Index of the minimum value, which indicates the most illuminated side. 
##This could be modified to take something else than the most illuminated side, depending on the user's needs. 
##Thoughts from ci-ete-sim group is needed whether it is necessary.
index=np.argmin(illumination)

##The chosen closest approach vector.
p_most=p_rotated[index]


##Testing it is unit length too, just to make sure
#print(np.sqrt(p_most[0]**2 + p_most[1]**2 + p_most[2]**2))


##Trajectory-vector of the satellite. Used for calculating its position during the fly-by
trajectory_vector=- sat_ini_vec + p_most


##Allocate the postionalData file, that Mario's code can read.
positionalData=np.array([["camera_x", "camera_y", "camera_z", "sun_x", "sun_y", "sun_z", "rot"]])




##Allocate camera's position coordinates of the fly-by
camera_xyz=np.zeros((2*n,3))


##Calculating the coordinates and adding them to the allocated array.
for j in range (2*n):
    camera_xyz[j]+=sat_ini_vec+j*trajectory_vector/n
    
    



##These next few lines just modify the array's in a way that the satellite trajectory-points and sun coordinates etc. are 
##appended to the positionalData array in a way that Mario's code wants it.
    
##If someone happens to modify the code, a note forth pointing out: These sun_x, sun_y, sun_z do not have anything to do with the sun's vector
## calculated above, these are simply used in Mario's code so that Blender can locate the sun somewhere.
## In blender it doesn't matter where the sun is but it has coordinate-inputs which need to be filled. 

##So tldr: do not worry about the next three variables at all.
sun_x=0.00000

sun_y=3.42020

sun_z=9.39693


inputs=np.array([[sun_x, sun_y, sun_z, rot]])

for k in range(len(camera_xyz)-1):
    inputs=np.append(inputs,np.array([[sun_x, sun_y, sun_z, rot]]),axis=0)



##This is the final product, used by Mario's code to simulate the fly-by. This array is written into both Excel and a text file.
positionalData=np.append(positionalData,(np.append(camera_xyz,inputs,axis=1)),axis=0)



##Writing the positionalData array to both Excel and a text file.

##Excel  
#pd.DataFrame(positionalData).to_excel('fly_by_coordinates.xlsx', header=False, index=False)

##Text file
textfile=open('fly_by_coordinates.txt','w')
for row in positionalData:
   row=np.reshape(row,(1,7)).tolist()
   string="            ".join(str(item) for item in row[0])
   
       
      
   textfile.write(string + '\n')
textfile.close()

print("fly_by_coordinates.txt written")



"""
##Plotting the satellites initial vector, closest approach vector and the fly-by trajectory.
##NOTE: only for visualization purposes, so that the user gets the idea behind these calculations. I will also make a diagram of the 
##why this calculates the trajectory.

fig=plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')




ax.plot([a,0],[b,0],[c,0],'k',label='satellites initial vector')

ax.plot([0, p_x],[0,p_y],[0, p_z],'g', label='non-rotated vector')
ax.plot([0,p_most[0]],[0,p_most[1]],[0,p_most[2]],label='closest approach vector')
ax.scatter(camera_xyz[:,0],camera_xyz[:,1],camera_xyz[:,2], c='r', marker='o',s=1.5,label='satellites trajectory')
ax.legend()

plt.show()
"""

