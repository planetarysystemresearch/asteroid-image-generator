"""
Test script for learning how to request a render from albedoPreviewer.
* Add two models, one a biconic asteroid, the other more rubbeduck-like.
"""
import bpy
import sys
import argparse

# ARGUMENT PARSING

argv = sys.argv

argv = argv[argv.index("--") + 1:]

parser = argparse.ArgumentParser()

# Why is it working without any arguments, when all of them are required?

parser.add_argument("--seed", dest = "seedValue", nargs = 1, required = True,
        help = "Seed value for the noise texture.")

parser.add_argument("--scale", dest = "scaleValue", nargs = 1, required = True,
        help = "Scale of the noise texture.")

parser.add_argument("--detail", dest = "detailValue", nargs = 1, required = True,
        help = "Detail of the noise texture.")

parser.add_argument("--roughness", dest = "roughnessValue", nargs = 1,
        required = True, help = "Roughness of the noise texture.")

parser.add_argument("--black", dest = "leftHandle", nargs = 1, required = True,
        help = "Position of the black handle of the colorRamp")

parser.add_argument("--white", dest = "rightHandle", nargs = 1, required = True,
        help = "Position of the white handle of the colorRamp")


args = parser.parse_args(argv)
#print(args)	




scene = bpy.context.scene
scene.render.filepath = "modules/albedo.png"

materialName = "Noise"

# Noise texture
noiseNode = bpy.data.materials[materialName].node_tree.nodes["Noise Texture"]

seedValue       =   noiseNode.inputs[1]
scaleValue      =   noiseNode.inputs[2]
detailValue     =   noiseNode.inputs[3]
roughnessValue  =   noiseNode.inputs[4]

# ColorRamp

colorRampNode = bpy.data.materials[materialName].node_tree.nodes["ColorRamp"]

#bpy.data.materials["Noise"].node_tree.nodes["ColorRamp"].color_ramp.elements[0].position

leftHandle  =   colorRampNode.color_ramp.elements[0] 
rightHandle =   colorRampNode.color_ramp.elements[1] 


"""
seedValue.default_value         =   1
scaleValue.default_value        =   5 
detailValue.default_value       =   2 
roughnessValue.default_value    =   .5
"""

seedValue.default_value         =   float(args.seedValue[0])
scaleValue.default_value        =   float(args.scaleValue[0])
detailValue.default_value       =   float(args.detailValue[0])
roughnessValue.default_value    =   float(args.roughnessValue[0])
leftHandle.position             =   float(args.leftHandle[0]) 
rightHandle.position            =   float(args.rightHandle[0])



# Rendering
bpy.ops.render.render(write_still = True)
