import bpy
import math
import mathutils
import os
import numpy as np

def sunSetupCartesian(sunObject, sun_x, sun_y, sun_z):
    """
    Updates the location and rotation of the sunlamp, with coordinates as input.
    """

    sunLocation = (sun_x, sun_y, sun_z)
    sunDirectionVector = mathutils.Vector(sunLocation)  # Assumes that the Sun points to (0,0,0)

    sunObject.location = sunLocation  # The location doesn't really matter with sunlamps
                                      # Distance should just modify the power of the lamp

    sunObject.rotation_mode = "XYZ"

    rot_quat = sunDirectionVector.to_track_quat("Z", "Y")
    sunObject.rotation_euler = rot_quat.to_euler()

def sunSetupSpherical(sunObject, sun_lat, sun_lon):
    """
    Updates the location and rotation of the sunlamp, in the ecliptical coordinate system.
    """

    sunObject.location = (0, 0, 0)  # Sunlamp position doesn't matter in Blender.
    sunObject.rotation_mode = "XYZ" # I guess?

    # Equivalences, decided by me:
    # Latitude 90º  ->  rot X 0
    # Latitude 0º   ->  rot X -90
    # Latitude -90º ->  rot X -180
    # Longitude 0   ->  rot Z 0
    # Longitude and rot Z are the same, I believe! Nice.

    sunObject.rotation_euler[0] = math.radians(sun_lat - 90)
    sunObject.rotation_euler[1] = math.radians(0)   # This one doesn't matter. Euler's lock?
    sunObject.rotation_euler[2] = math.radians(sun_lon)

def cameraSetupCartesian(cameraObject, camera_x, camera_y, camera_z):
    """
    Updates the location and rotation of the camera, with coordinates and
    a reference (is this the correct term in python?) to the camera as input.
    """

    cameraLocation = (camera_x, camera_y, camera_z)
    cameraDirectionVector = mathutils.Vector(cameraLocation)    # Assuming pointing to (0,0,0)

    cameraObject.location = cameraLocation

    cameraObject.rotation_mode = "XYZ"
    rot_quat = cameraDirectionVector.to_track_quat("Z", "Y")
    cameraObject.rotation_euler = rot_quat.to_euler()

def cameraSetupSpherical(cameraObject, camera_lat, camera_lon, camera_dist):
    """
    Updates the location and rotation of the sunlamp, in the ecliptical coordinate system.
    camera_lat and camera_lon in degrees, please, and camera_dist in km.
    """

    # It will still point to the origin always, but the position will be given as an
    # angle and a distance

    # With this approach I would require beta as an input!
    # No, lies, he will give me the distance. That's all.

    camera_x = math.sin(math.radians(camera_lon)) * camera_dist
    camera_y = math.cos(math.radians(camera_lon)) * camera_dist
    camera_z = math.sin(math.radians(camera_lat)) * camera_dist

    cameraLocation = (camera_x, camera_y, camera_z)
    cameraDirectionVector = mathutils.Vector(cameraLocation)    # Assuming pointing to (0,0,0)

    cameraObject.location = cameraLocation 

    cameraObject.rotation_mode = "XYZ"
    rot_quat = cameraDirectionVector.to_track_quat("Z", "Y")
    cameraObject.rotation_euler = rot_quat.to_euler()

def targetSetup(targetName, satellite = True):
    """
    Loops around the "Targets" collection and hides every object except
    the chosen target, in both viewport and render views. 
    """

    targetsCollection = bpy.data.collections.get("Targets")

    parents = (x for x in targetsCollection.objects if not x.parent)

    for object in parents:

        if object.name == targetName:

            object.hide_viewport = False
            object.hide_render = False

            # And the same for its children, if it has any!
            #if object.children:
            if object.children and satellite:
                for child in object.children:
                    child.hide_viewport = False
                    child.hide_render = False

            if object.children and not satellite:
                for child in object.children:
                    child.hide_viewport = True
                    child.hide_render = True

        else:
            # Children must be disabled when not in use   <-- lol

            object.hide_viewport = True
            object.hide_render = True
            
            if object.children:
                for child in object.children:
                    child.hide_viewport = True
                    child.hide_render = True

def proceduralDetail(targetName, state):
    """
    Activates/deactivates the procedural detail in the render.
    "state" should be a boolean.
    """
    
    if not state:

        targetObject = bpy.data.objects[targetName]
        modifier = targetObject.modifiers["GeometryNodes"]
        #modifier.show_render = state

        # "Input_3" is the density value. I'd love to find it by name, but.
        modifier["Input_3"] = 0.0

        # What about the children? This has been wrong for a long time.

        if targetObject.children:
            childrenModifier = targetObject.children[0].modifiers["GeometryNodes"]
            childrenModifier["Input_3"] = 0.0



def systemRotation(targetName, angle):
    """
    Rotates the main body and its satellite. Angle in degrees, represents
    Didymoon's rotation. Didymain rotates accordingly.
    """

    targetObject = bpy.context.scene.objects[targetName]

    ratio = 5.27    # D2's orbital period / D1's rotation period.

    # First: rotate D1 by the angle times the ratio. D1 will rotate as well,
    # because it's a child object.

    targetObject.rotation_euler[2] = math.radians(angle*ratio)

    # Second: rotate D2 back by to the desired angle.

    if targetObject.children:

        targetObject = targetObject.children[0]

    correction = angle - (angle*ratio)

    targetObject.rotation_euler[2] = math.radians(correction)

def sunIrradiance(sunObject, irradiance):
    """
    Sets the irradiance of the Sun lamp (in W/m^2) to the desired value.
    """

    sunObject.data.energy = irradiance


def adjustMaterial():
    """
    This function should access the selected material and change their k/w values.
    """

    pass

# I think that everything under here can go.

def sunDistanceToIrradiance(sunObject, distance):
    """
    Sets the power of the Sun lamp (in W/m^2) according to the distance to the Sun.
    Distance in meters?
    UNTESTED!
    """
    
    solarRadius = 6.957e8   # m
    solarSurfaceIrradiance = 64e6    # W/m²

    sunObject.data.energy = ( solarRadius ** 2 / distance ** 2 ) * solarSurfaceIrradiance

def sunSpectralIrradiance(wavelength):
    """
    Calculates the solar irradiance for the required wavelength.
    Data from 2000 ASTM Standard Extraterrestrial Spectrum Reference E-490-00.
    At 1 AU I think!
    """

    lambdas, spIrradiances = [], []
    filepath = os.path.join(os.path.dirname(bpy.data.filepath), 
            "data/spectralSolarIrradiance.txt")

    with open(filepath) as f:
        next(f)     # Skips the header
        next(f)     # Two lines in this case
        for line in f:
            items = [float(x) for x in line.split(',')]
            lambdas.append(items[0])
            spIrradiances.append(items[1])

    return np.interp(wavelength, lambdas, spIrradiances)

def wavelengthToAlbedo(wavelength, filename):
    """
    Returns the albedo corresponding to a certain wavelength.
    filename should be a string containing relative path.
    """

    lambdas, albedos = [], []
    filepath = os.path.join(os.path.dirname(bpy.data.filepath), 
            filename)
            #"data/S-type-asteroid-Bus-DeMeo.txt")

    with open(filepath) as f:
        next(f)     # Skips the header
        for line in f:
            items = [float(x) for x in line.split('\t')]
            lambdas.append(items[0])
            albedos.append(items[1])

    return np.interp(wavelength, lambdas, albedos)
