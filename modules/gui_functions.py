import subprocess
import numpy as np
import PySimpleGUI as sg
import matplotlib.pyplot as plt
from matplotlib import rcParams
from mpl_toolkits import mplot3d

# A problem with my gui funcion(s) is that it includes the end bracket.
# Great because I can forget about them, but now I can't add stuff on the same line.

# LAYOUT FUNCTIONS ###############################################################################

# I think I have to set defaults here that will then be overwritten at gui.py,
# otherwise the defaults of the functions will not know what to do. Seems suboptimal.

defaultTextSize     =   (28,1)
defaultSubtextSize  =   (5, 1)
defaultBoxSize      =   (10,1)

defaultFont         =   "Arial 12"

def show(key, visible, layout):
    """
    Helper function that creates a Column that can be later made hidden, thus appearing "collapsed"
    """

    # Is the key needed?
    ## Ah, yes, you call the key for updating the section.

    return [sg.pin(sg.Column(layout, key=key, visible = visible, pad=(0,0)))]

    # So I stillhave to add the [] to the input of the show function? Didn't I solve this?

def separator():

    return [sg.Text("")] 

def title(text, font = defaultFont + " bold"):

    # Font not doing anything here either?
    # Ah, if I add a font type before it does!

    #return [sg.Text(text, font = font)]
    return [sg.Text(text, font = font)]

def fileSelector(text, default, key, size = defaultTextSize, font = defaultFont, tooltip = ""): 
    """
    Text, followed by input box, followed by file browser button.
    Optional size, font, and tooltip.
    """

    return [sg.Text(text, size = size, font = font, tooltip = tooltip), 
            sg.InputText(default, key = key),
            sg.FileBrowse()]

def fileSelectorPlusButton(text, default, key, buttonText, size = defaultTextSize, 
        font = defaultFont, tooltip = ""): 
    """
    Text, followed by input box, followed by file browser button, followed by a button.
    Optional size, font, and tooltip.
    """

    return [sg.Text(text, size = size, font = font, tooltip = tooltip), 
            sg.InputText(default, key = key),
            sg.FileBrowse(),
            sg.Button(buttonText)]

def folderSelector(text, default, key, size = defaultTextSize, font = defaultFont, tooltip = ""): 
    """
    Text, followed by input box, followed by folder browser button.
    Optional size, font, and tooltip.
    """

    return [sg.Text(text, size = size, font = font, tooltip = tooltip), 
            sg.InputText(default, key = key),
            sg.FolderBrowse()]

def checkbox(text, default, key, size = defaultTextSize, font = defaultFont):

    # Why can't I change the size of the checkbox? 
    ## Seems impossible, you'd have to make your own image. So stupid.

    return [sg.Text(text, size = size, font = font),
           sg.Checkbox("", enable_events = True, size = (50, 50), key = key, default = default)
           ]

def dropdown(text, options, default, key, size = defaultTextSize, boxSize = defaultTextSize, 
        font = defaultFont):
    """
    "options" is a list of strings.
    """

    return[
        sg.Text(text, size = size, font = font),
        sg.Combo(options, default_value = default, font = font, readonly = True,
            size = boxSize, enable_events = True, key = key)
            ]


def slider(text, default, key, valueRange, resolution = .1, textSize = defaultTextSize, boxSize = defaultBoxSize, 
        font = defaultFont):

    return [sg.Text(text, size = textSize, font = font),
            sg.Slider(range = valueRange, orientation = "h", size = boxSize,
                resolution = resolution, default_value = default, key = key,
                enable_events = True),
            ]

def singleInput(text, default, key, size = defaultTextSize, boxSize = defaultBoxSize, 
        font = defaultFont, disabled = False, justification = "center"):

    # Is this needed, when one could use multipleInputs() with just one entry?

    # TBD: Ability to make edit false.
    # TBD, if possible: accept only a certain type.

    return [sg.Text(text, size = size, font = font),
            sg.Input(default, justification = justification, key = key, size = boxSize, 
                font = font, disabled = disabled)]


def multipleInputs(title, *variables, titleSize = defaultTextSize, 
        subtextSize = defaultSubtextSize, boxSize = defaultBoxSize, 
        font = defaultFont, justification = "center"):
    # Maybe call titleSize textSize?
    """
    "variables" format: (name, default, key)
    """

    # TBD, if possible: accept only a certain type.

    command = []

    if title != "":

        command += [sg.Text(title, size = titleSize, font = 50)]
    
    for variable in variables:

        command += [sg.Text(variable[0], size = subtextSize), 
                    sg.Input(variable[1], justification = justification, 
                    key = variable[2], size = boxSize, font = font,
                    enable_events = True),
                    sg.Text("   "),
                    ]

    return command

def frame(title, layout):
    """
    Draws a frame around the layout.
    """

    return [sg.Frame(title = title, layout = layout)]
                    
def plotAndSave(*plots, title, xLabel, yLabel, savePath, saveTitle):
    """
    Attempt to make plotAndSave work with multiple plots same figure.
    "plots" format: (xArray, yArray, optional extras).
    xArray and yArray are arrays, ofc. I'm using numpy arrays, probably others work too.
    The rest of the input variables are strings.
    """

    # Generating plots, whether they have two or more variables,
    # and plotting them on top of each other.

    for item in plots:

        # How to make it so that it reads 2+ variables?
        # There must be a better way, for sure.
        # But it WORKS!

        argument = ""
        for i, variable in enumerate(item):
            argument += f"item[{i}], "

        command = f"plt.plot({argument})"
        exec(command)

    plt.title(title)
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)

    # Saving as image
    name = f"{savePath}/{saveTitle}.png"
    plt.savefig(name)
    print(f"{name} saved.")


def saveRawData(xArray, yArray, savePath, saveTitle):

    # Saving values
    name = f"{savePath}/{saveTitle}.txt"
    # np.c_ makes the arrays into columns instead of rows.
    np.savetxt(name, np.c_[xArray, yArray])
    
    print(f"{name} saved.")


def renderAlbedoPreview(blenderPath, noiseSeed, noiseScale, noiseDetail, 
        noiseRoughness, noiseBlackLevel, noiseWhiteLevel):
    """
    Example:
    /home/mario/Programs/blender-3/./blender --background modules/albedoPreviewer.blend --python modules/albedoPreviewerControl.py -- --seed 1 --scale 0 --detail 2 --roughness .5 --black .5 --white .9

    Will the paths cause problems because this file is in modules/?
    """
    
    command = [
            blenderPath,
            "--background", "modules/albedoPreviewer.blend",
            "--python",     "modules/albedoPreviewerControl.py",
            "--",
            "--seed",       str(noiseSeed),
            "--scale",      str(noiseScale),
            "--detail",     str(noiseDetail),
            "--roughness",  str(noiseRoughness),
            "--black",      str(noiseBlackLevel),
            "--white",      str(noiseWhiteLevel),
            ]

    #print(command)
    print("Rendering preview")
    subprocess.run(command)

def disablePostInputs(window, inputsList, disabledList):

    # Testing
    #window["surfaceAlbedo"].update(disabled = True)

    for item in inputsList:
        if item in disabledList:
            window[item].update(disabled = True)
        else:
            window[item].update(disabled = False)


# TEST

from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
from matplotlib.patches import FancyArrowPatch

class Arrow3D(FancyArrowPatch):
# From https://stackoverflow.com/questions/38194247/how-can-i-connect-two-points-in-3d-scatter-plot-with-arrow

    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs
        
    # Test inside the test

    def do_3d_projection(self, renderer=None):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, self.axes.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))

        return np.min(zs)

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)


def drawMap(cameraCoordinates, sunCoordinates, angular = False):
    """
    cameraCoordinates: list of tuples containing three coordinates.
    """

    # TEST

    def toCartesian(lat, lon, dist):
        # Numpy uses radians, careful!

        lat = np.radians(lat)
        lon = np.radians(lon)

        x = dist * np.cos(lat) * np.cos(lon)
        y = dist * np.cos(lat) * np.sin(lon)
        z = dist * np.sin(lat)

        return [x, y, z]

    asteroidRadius  =   .4  # km
    cameraSize      =   100
    cameraColor     =   "green"
    labelOffset     =   .5
    sunArrowLength  =   3   # lenght = this number times the unit vector

    axisColor = "white"

    # This fucks up the post-processing plots...
    ## Put them back to black at the end and it should be fine.
    rcParams["xtick.color"] = axisColor
    rcParams["axes.labelcolor"] = axisColor

    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})

    fig.set_facecolor('black')
    ax.set_facecolor('black') 

    ax.w_xaxis.pane.fill = False
    ax.w_yaxis.pane.fill = False
    ax.w_zaxis.pane.fill = False

    ax.w_xaxis.line.set_color(axisColor)
    ax.w_yaxis.line.set_color(axisColor)
    ax.w_zaxis.line.set_color(axisColor)

    ax.set_xlabel("X (km)")
    ax.set_ylabel("Y (km)")
    ax.set_zlabel("Z (km)")

    ax.set_box_aspect([1,1,1])

    # Asteroid

    u = np.linspace(0, 2 * np.pi, 100)
    v = np.linspace(0, np.pi, 100)

    x = asteroidRadius * np.outer(np.cos(u), np.sin(v))
    y = asteroidRadius * np.outer(np.sin(u), np.sin(v))
    z = asteroidRadius * np.outer(np.ones(np.size(u)), np.cos(v)) 
    
    ax.plot_surface(x, y, z,  rstride=4, cstride=4, color='gray', linewidth=0, alpha=1)

    # Cameras

    maxCoord = 0

    for i, coords in enumerate(cameraCoordinates):

        if angular:

            coords = toCartesian(coords[0], coords[1], coords[2]) 
            print(coords)
        
        x, y, z = coords       

        maxCoord = max(maxCoord, max(abs(x), abs(y), abs(z)))
        
        ax.scatter(x, y, z, color = cameraColor, s = cameraSize)
        ax.text(x+labelOffset, y+labelOffset, z+labelOffset, i+1, color = cameraColor)


	# Sun 
    # Make it calculate the angle and then select the arrow length you want,
    # in case the values on the datafile are stupidly large.

    if angular:

        # TEST, NEW, CHECK!
        sunCoordinates = toCartesian(sunCoordinates[0], sunCoordinates[1], 1)
        #print(sunCoordinates)

    normalizedSun = sunCoordinates/np.linalg.norm(sunCoordinates)

    # Probably the length of the arrow should depend on the ax size

    origin  =   normalizedSun * sunArrowLength
    dest    =   normalizedSun

    arw = Arrow3D([origin[0],dest[0]],[origin[1],dest[1]],[origin[2],dest[2]],
            arrowstyle="->", color = "Orange", lw = 3, mutation_scale=25)

    ax.add_artist(arw)


    axLim = max(maxCoord * 1.1, 1)


    #ax.set_aspect("equal") # Not working for me :C
    # Not working either (limits set correctly, but the aspect ratio is wrong.
    ax.auto_scale_xyz([-axLim, axLim], [-axLim, axLim], [-axLim, axLim])    

    plt.show()
    
    # Putting this back to how it was
    rcParams["xtick.color"] = "black"
    rcParams["axes.labelcolor"] = "black"
