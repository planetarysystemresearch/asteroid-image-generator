import PySimpleGUI as sg
from modules import gui_functions as gui
from PIL import Image, ImageTk, ImageDraw, ImageOps

postTextSize        =   (12, 1)
postSubtextSize     =   (25, 1)
postBoxSize         =   (5, 1)

# Rendering tab #########################################################

## Left column #########################

def cartesianSubsection(settings):
    """
    Selection of X, Y, Z for sun and camera, in two lines.
    """

    subsection = [

        gui.multipleInputs("Sun coordinates (km):", 
            ("X", settings.sunCoordsCart[0], "sunX"),
            ("Y", settings.sunCoordsCart[1], "sunY"),
            ("Z", settings.sunCoordsCart[2], "sunZ"),
            subtextSize = (1,1), boxSize = (6,1)),

        gui.multipleInputs("Camera coordinates (km):",
            ("X", settings.cameraCoordsCart[0], "camX"),
            ("Y", settings.cameraCoordsCart[1], "camY"),
            ("Z", settings.cameraCoordsCart[2], "camZ"),
            subtextSize = (1,1), boxSize = (6, 1)),
        ]

    return subsection

def angularSubsection(settings):
    """
    Selection of (Lat, Lon) for the sun, and (Lat, Lon, Dist) for camera.
    """
    subsection = [

        gui.multipleInputs("Sun coordinates (°):",
            ("lat", settings.sunCoordsAng[0], "sunLat"),
            ("lon", settings.sunCoordsAng[1], "sunLon"),
            subtextSize = (4,1), boxSize = (6, 1)),

        gui.multipleInputs("Camera coordinates (°, km):",
            ("lat", settings.cameraCoordsAng[0], "camLat"),
            ("lon", settings.cameraCoordsAng[1], "camLon"),
            ("dist", settings.cameraCoordsAng[2], "camDist"),
            subtextSize = (4,1), boxSize = (6, 1)),
        ]

    return subsection

def renderingTabColumn1(settings, toggle_customOutput, toggle_cartesian, toggle_coordsFile, defaultCoordType):

    content = [
        gui.title("PATHS"),

        # Blender executable selection.

        gui.fileSelector("Blender executable path:", default = settings.blenderPath, 
            key = "-BLENDERPATH-"),

        # Custom output checkbox.

        gui.checkbox("Custom output folder name?", default = toggle_customOutput, key = "output"),

        # Custom path selection, shown only if needed.

        gui.show("customOutputSection", toggle_customOutput,
            #[gui.folderSelector("Output path:", default = settings.outDir, key = "-OUTDIR-")]
            [gui.singleInput("Output folder name:", default = settings.outDir,
                key = "-OUTDIR-")]
            ),

        gui.separator(), gui.title("COORDINATES"),

        # "From file" checkbox.

        gui.dropdown("Coordinate type:", ["Cartesian", "Angular"],
            default = defaultCoordType, key = "coordinateType"),

        gui.checkbox("Coordinates from file?", default = settings.fromFile, key = "file"),

        gui.show("fileCoordSection", settings.fromFile,
            [gui.fileSelector("File path:", default = settings.positionsPath, key = "-COORDSFILE-")]
            ),

        
        gui.show("coordSection", not settings.fromFile,

            [gui.show("cartesianSubsection", toggle_cartesian, cartesianSubsection(settings)),
            gui.show("angularSubsection", not toggle_cartesian, angularSubsection(settings))]

            ), 

        [sg.Button("Preview geometry")],

        gui.separator(), gui.title("CAMERA"), 

        gui.dropdown("Presets:", ["None", "ASPECT-VIS", "ASPECT-NIR", "ASPECT-VIS-Green"],
            default = "None", key = "camera"),

        # This doesn't work correctly now if defaultCamera != None

        gui.multipleInputs("Camera resolution (px):", 
            ("X", settings.resX, "XRes"), 
            ("Y", settings.resY, "YRes"),
            subtextSize = (1,1), boxSize = (6, 1)),

        gui.multipleInputs("Field of view (rad):",
            ("X", settings.fovX, "XFoV"),
            ("Y", settings.fovY, "YFoV"),
            subtextSize = (1,1), boxSize = (6, 1)),
        
        gui.separator(),

        # Target selection

        gui.title("TARGET"),

        gui.dropdown("Target:", ["DRAv3.03", "BISv1.0", "RISv1.0", "RISv1.0_D2centered", "Sphere", "67P", "DART_D1_centered", "DART_D2_centered", "Empty"],
            default = settings.target, key = "target"),

        gui.checkbox("Procedural detail?", default = settings.detail, key = "detail"),

        gui.checkbox("Show satellite?", default = True, key = "satellite"),

        # Scale and rotation

        gui.singleInput("Scale:", default = settings.scale, key = "scale", boxSize = (5,1)),

        gui.singleInput("Rotation (°):", default = settings.rotation, key = "rot", boxSize = (5,1), disabled = toggle_coordsFile),

        ]

    return content

## Right column #############################

def albedoVarSubsection(settings):
    """
    Menu for selecting all noise parameters and updating the preview.
    """

    subsection = [[
        sg.Column([
        # First column with albedo values

            gui.slider("Albedo variation (± %):", default = 15, key = "albedoRange",
                valueRange = (0, 100), resolution = 1, boxSize = (10, 20)),

            gui.singleInput("Noise scale:", default = 5, key = "noiseScale"),

            gui.slider("Seed:", default = 0, key = "noiseSeed",
                valueRange = (0, 99), resolution = 1, boxSize = (10, 20)),

            gui.slider("Detail:", default = 15, key = "noiseDetail", 
                valueRange = (0, 15), boxSize = (10, 20)),

            gui.slider("Roughness:", default = .5, key = "noiseRoughness", 
                valueRange = (0, 1), boxSize = (10, 20)),


            gui.slider("Black level:", default = .45, key = "noiseBlackLevel",
                valueRange = (0,1), resolution = .05, boxSize = (10, 20)),
            
            gui.slider("White level:", default = .6, key = "noiseWhiteLevel",
                valueRange = (0,1), resolution = .05, boxSize = (10, 20)),

            [sg.Button("Update preview")],

            ], vertical_alignment = "t"),

        sg.Column([
        # Second column with albedo preview.
        # Why's the text not centered? (sg.Push didn't solve it)

            [sg.Image(source = "modules/albedo_preview.png", key = "-ALBEDOPREVIEW-")],
            [sg.Text("Albedo distribution preview", size = (28, 1), font = "Arial 8")], 
            [sg.Text("Pure black -> minimum albedo.", size = (28, 1), font = "Arial 8")], 
            [sg.Text("Pure white -> maximum albedo.", size = (28, 1), font = "Arial 8")], 
            ], element_justification = "c", vertical_alignment = "t")

        ]]

    return subsection

def renderingTabColumn2(settings, toggle_ROLO, toggle_McEwen):

    content = [

        gui.title("MATERIAL"),

        gui.dropdown("Photometric function (Surface):",
            ["Lommel-Seeliger", "ROLO", "McEwen", "Lambert"],
            default = settings.surfaceFunction, key = "surfaceFunction"),

        gui.dropdown("Photometric function (Boulders):",
            ["Lommel-Seeliger", "ROLO", "McEwen", "Lambert"],
            default = settings.boulderFunction, key = "bouldersFunction"),

        #gui.show("ROLOSection", False, 
        gui.show("ROLOSection", toggle_ROLO, 

            [gui.frame(title = "ROLO parameters",
                layout = [

                    gui.multipleInputs("",
                        ("G0", settings.ROLOG0, "ROLOG0"),
                        ("G1", settings.ROLOG1, "ROLOG1"),
                        ("A0", settings.ROLOA0, "ROLOA0"),
                        ("A1", settings.ROLOA1, "ROLOA1")),

                    gui.multipleInputs("",
                        ("A2", settings.ROLOA2, "ROLOA2"),
                        ("A3", settings.ROLOA3, "ROLOA3"),
                        ("A4", settings.ROLOA4, "ROLOA4")),
                    ]
                )]
            ),

        gui.show("McSection", toggle_McEwen,

            [gui.frame(title = "McEwen parameters",
                layout = [


                    gui.multipleInputs("",
                        ("ε", settings.McEEpsilon, "McEEpsilon"),
                        ("ξ", settings.McEKsi, "McEKsi"),
                        ("η", settings.McEEta, "McEEta"),
                        ),

                    gui.multipleInputs("",
                        ("β", settings.McEBeta, "McEBeta"),
                        ("γ", settings.McEGamma, "McEGamma"),
                        ("δ", settings.McEDelta, "McEDelta")
                        )
                    ]
                )]
            ),

        gui.separator(),

        gui.singleInput("Image albedo:", default = settings.imageAlbedo, 
            key = "albedo"),

        gui.checkbox("Albedo variations?", default = False, key = "albedoVar"),

        #gui.show("albedoVarSubsection", False, albedoVarSubsection)
        gui.show("albedoVarSubsection", False, albedoVarSubsection(settings))

        ]

    return content

# Rendering tab proper

def renderingTab(settings, toggles, defaultCoordType):

    #tab1 = renderingTabColumn1(settings, toggle_customOutput, toggle_cartesian, toggle_coordsFile, defaultCoordType)
    tab1 = renderingTabColumn1(settings, toggles["customOutput"], toggles["cartesian"], toggles["coordsFile"], defaultCoordType)

    #tab2 = renderingTabColumn2(settings, toggle_ROLO, toggle_McEwen)
    tab2 = renderingTabColumn2(settings, toggles["ROLO"], toggles["McEwen"])

    content = [

        # Columns 
        [sg.Column(tab1, element_justification = "l", vertical_alignment = "t"),
        sg.Column(tab2, element_justification = "l", vertical_alignment = "t")],

        # Buttons on the bottom
        [sg.Push(), sg.Button("Preview"), sg.Button("Render"), sg.Push()],

    ]

    return content

# Post-processing tab #########################################################

def recipeSelectionSection(recipesList):

    section = [

        gui.frame(
            title = "Recipes",
            layout = [

                gui.dropdown("Post-processing recipes:", 
                    recipesList,
                    default = "Choose one", key = "recipe", size = (30, 1)),

                [sg.Text("", font = "Arial 12 italic",
                    key = "-RECIPEDESCRIPTION-")],

                ]),

        ]

    return section

def inputDataSelectionSection():

    section = [
        gui.frame(
            title = "DATA",
                    
            layout = [

                # Having the last render as default would be rad.
                gui.folderSelector("Input folder:", default =  "Renders/test/Original", 
                    key = "-POSTINPUTFOLDER-"),

                gui.fileSelector("Target spectra:",
                    default = "modules/data/S-type-asteroid-spectra.txt",
                    key = "-TARGETSPECTRA-"),

                gui.fileSelector("Quantum efficiencies:",
                    default = "modules/data/ASPECT-NIR-quantum-efficiency.txt",
                    key = "-QUANTUMEFFICIENCIES-"),

                [sg.Button("Load render metadata")],

                ]
            ),
        ]

    return section

def postParamsSection(settings):
    """
    Boxes with all the parameters for the different post-processing recipes.
    They stay in place for every recipe, but get activated or deactivated as needed.
    """

    section = [

        gui.title("Parameters:"),
        # Careful, don't mix post-processing variable names with rendering variable names!
        
        gui.multipleInputs("Camera:",
            ("X Res (px)", 640, "post_XRes"), 
            ("Y Res (px)", 512, "post_YRes"),
            ("X FoV (rad)", 0.116937, "post_XFoV"),
            ("Y FoV (rad)", 0.0942478, "post_YFoV"),
            titleSize = postTextSize, subtextSize = postSubtextSize, boxSize = postBoxSize),

        gui.multipleInputs(" ",
            ("Aperture Ø (mm)", 13.6,  "apertureDiameter"),
            ("Optics Trans.", 0.9, "opticsTransmission"),
            titleSize = postTextSize, subtextSize = postSubtextSize, boxSize = postBoxSize),

        gui.multipleInputs("Illumination:",
            ("Sun distance (AU)", 1, "distanceToSun"),
            ("surfaceAlbedo", 0.04,  "surfaceAlbedo"),
            ("Image albedo", .4, "imageAlbedo"),
            titleSize = postTextSize, subtextSize = postSubtextSize, boxSize = postBoxSize),

        gui.multipleInputs("Spectral:",
            ("Sp. transmission Strength", 0.3, "spectralTransmissionStrength"),
            ("Sp. trans. Window Width (nm)", 25, "spectralTransmissionWindowWidth"),
            ("Initial wavelength (nm)", 700, "wlStart"),
            ("Final wavelength (nm)", 1800, "wlEnd"),
            titleSize = postTextSize, subtextSize = postSubtextSize, boxSize = postBoxSize),

        gui.multipleInputs("Other:", 
            ("readNoise", 15,  "readNoise"),
            ("darkCurrent", 1.0,  "darkCurrent"),
            ("integrationTime", 20,  "integrationTime"),
            ("fullWellCapacity", 15000,  "fullWellCapacity"),
            titleSize = postTextSize, subtextSize = postSubtextSize, boxSize = postBoxSize),

        gui.multipleInputs(" ", 
            ("snrWl", 800, "snrWl"),
            ("Sample wl start (nm)", 800, "wlSampleStart"),
            ("Sample wl end (nm)", 1700, "wlSampleEnd"),
            ("Sample wl step (nm)", 50, "wlSampleStep"),
            titleSize = postTextSize, subtextSize = postSubtextSize, boxSize = postBoxSize),

        gui.separator(),
        ]

    return section

def selectionBoxSubsection(settings):
    """
    Selection of box size in pixels, with two input boxes, and of box position,
    with two sliders.
    """

    subsection = [

        gui.title("Selection box"),

        gui.multipleInputs(
            "Box size (px):",
            ("x", "100", "xBox"),
            ("y", "100", "yBox"),
            titleSize = (20, 1), subtextSize = (1, 1), boxSize = postBoxSize),

        gui.slider("Box X position", default = 50, key = "boxXPosition", 
            valueRange = (0, 100), resolution = 1, boxSize = (10, 20)),

        gui.slider("Box Y position", default = 50, key = "boxYPosition", 
            valueRange = (0, 100), resolution = 1, boxSize = (10, 20)),

        ]

    return subsection



def imageAnalysisSection(settings):

    """
    column1 = [

        # Can I use a folder selector here and point it by default to whatever is selected on top?
        gui.fileSelectorPlusButton("Image:", default = "Renders/Demo/Original/frame-1.png", 
            key = "-IMAGESELECTION-", buttonText = "Load image", size = (6, 1)),

        # Test
        [sg.Button("Pop-up")],

        gui.show("selectionBoxSubsection", False, selectionBoxSubsection(settings))

        ]

    column2 = [

        [sg.Push(), 
            sg.Image(source = "", key = "-SPECTRALIMAGE-"),
            sg.Push()],
        ]

    section = [

        [sg.Column(column1, vertical_alignment = "t"),
            sg.Column(column2, vertical_alignment = "t")],
            ]

    return section
    """
    return [[sg.Button("Pop-up")]]

def runPostSection():

    section = [
        gui.separator(),

        gui.singleInput("Output folder name:   ../", default = "post", 
            key = "-POSTOUTPUTFOLDER-", size = (18, 1), boxSize = (28, 1), justification = "left"),

        [sg.Button("Run"), ],
        ]

    return section

def postprocessingTab(settings, recipesList, toggles):

    recipes = recipeSelectionSection(recipesList)

    inputs = inputDataSelectionSection()

    params = postParamsSection(settings)

    imageSelection = imageAnalysisSection(settings)

    run = runPostSection()

    content = [

        [sg.Column(recipes, element_justification  = "l", vertical_alignment = "t"), 
        sg.Column(inputs, element_justification = "l", vertical_alignment = "t")],

        gui.show("postParameters", toggles["postParameters"], params), 

        gui.show("imageAnalysisSection", toggles["imageAnalysis"], imageSelection),

        gui.show("runButton", toggles["postParameters"], run),

        ]

    return content

def spectralRetrievalPopup():

    # The two columns should remain hidden until an image is loaded.

    column1 = [
        #gui.title("Show image size and stuff here?"),

        #[sg.Text(f"Image dimensions: {")],

        gui.slider("Preview size", default = ".5", key = "preview_scale", 
                   valueRange = (.1, 1), resolution = .1, boxSize = (10, 20)),

        gui.title("Selection box"),

        gui.multipleInputs(
            "Box size (px):",
            ("x", "100", "xBox"),
            ("y", "100", "yBox"),
            titleSize = (20, 1), subtextSize = (1, 1), boxSize = postBoxSize),

        gui.slider("Box X position", default = 50, key = "boxXPosition", 
            valueRange = (0, 100), resolution = 1, boxSize = (10, 20)),

        gui.slider("Box Y position", default = 50, key = "boxYPosition", 
            valueRange = (0, 100), resolution = 1, boxSize = (10, 20)),
            ]

    column2 = [
        [sg.Push(), 

            # This should only appear after an image is loaded, right?
            sg.Image(source = "", key = "-POPUP_SPECTRALIMAGE-"), 
            sg.Push()],
        ]


    popup = [

        gui.fileSelectorPlusButton("Image:", default = "Renders/Demo/Original/frame-1.png", 
            key = "-IMAGESELECTION-", buttonText = "Load image", size = (6, 1)),

        #[sg.Column(column1, vertical_alignment = "t"),
        #    sg.Column(column2, vertical_alignment = "t")],


        gui.show("popup_parameters", False, 
                #[gui.title("Test")],
                [[sg.Column(column1, vertical_alignment = "t"),
                sg.Column(column2, vertical_alignment = "t")]],
                ),

        sg.Button("Close")
            ],

    return popup
