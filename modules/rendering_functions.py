import bpy
import time     # For adding the time and date to the filenames.
from os.path import join
from os import getcwd
from modules import setup_functions

def renderSingleImage(scene, resolutionX, resolutionY, foldername, filename):
#def renderSingleImage(scene, resolutionX, resolutionY, filename):
    """
    Also number of samples in Cycles maybe?
    And denoising option?
    Also filmic space and stuff?
    """

    scene.render.image_settings.color_mode = 'RGB'
    scene.render.engine = 'CYCLES'

    # Double slash == Parent directory
    # AP: Always assume paths relative to parent directory
    scene.render.filepath = join(getcwd(),foldername, filename)
    #scene.render.filepath = join("//Renders/", filename)

    scene.render.image_settings.file_format = 'PNG'
    scene.render.resolution_x = resolutionX
    scene.render.resolution_y = resolutionY

    bpy.ops.render.render(write_still=True)

def renderAnimation(scene, resolutionX, resolutionY, foldername, filename):
    """
    Renders the animation into a folder named after the target name, time and date, 
    every frame a picture

    New strategy: this function calls the function above (renderSingleImage()) in a loop.
    """

    # Is the following needed? If it's calling the above function anyways?

    scene.render.image_settings.color_mode = 'RGB'
    scene.render.engine = 'CYCLES'

    scene.render.image_settings.file_format = 'PNG'
    scene.render.resolution_x = resolutionX
    scene.render.resolution_y = resolutionY
    
    # Old method: simply rendering the animation. Blender added numbers after the
    # filenames, and there was no way around that.

    #scene.render.filepath = join("//Renders/", foldername, filename) 
    #bpy.ops.render.render(animation = True)

    # New method: calling renderSingleImage() on a loop.
    # Now I have total control over the filenames.

    for frame in range(scene.frame_start, scene.frame_end + 1):
        scene.frame_set(frame)
        #renderSingleImage(scene, resolutionX, resolutionY, foldername, filename)
        renderSingleImage(scene, resolutionX, resolutionY, foldername, filename + "-" + str(frame))

def renderMultispectralAnimation(scene, resolutionX, resolutionY, targetName):
    """
    Renders each frame of the animation with a different albedo.
    Wait a minute, it may be easier to call renderAnimation() multiple times.
    """
    pass

def renderCalibrationDisks(scene, resolutionX, resolutionY, foldername, filename, 
        filepath = False, ):

        # If single image -> One calibration image with those coordinates
        # If file (cartesian or angular) -> one per camera change, or sun direction change, maybe intensity too?

    if not filepath:

        # In theory I should set up again the original target after this, but if the
        # calibration step is always performed last, then it won't matter.

        setup_functions.targetSetup("CalibrationDisk")


        renderSingleImage(scene, resolutionX, resolutionY, foldername, filename + "-calibration")

    else:
        
        
        # For now: a render per image.
        setup_functions.targetSetup("CalibrationDisk")
        renderAnimation(scene, resolutionX, resolutionY, foldername, filename + "-calibration")

        """
        # Here: read the file, run this once per camera change, or sun direction change.
        

        with open(filepath) as f:

            lines = f.readlines()
        
            previousLine = ["?"]

            for line in lines[1:]:

                print(line)
                print(line[0])

                if line[0] == previousLine[0]:

                    print("Bam!")
                    # Don't know why it doesn't work.

                previousLine == line
        """  
