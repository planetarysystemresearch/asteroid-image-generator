def blurMotion(foldername, blurMin, blurMax):

    def applyMotionBlur(img, blurAmount, angle):
        """
        Takes a cv2 image, returns it all blurred up.
        """
        
        # Kernel (from stack overflow, I don't understand it at all)
        k = np.zeros((blurAmount, blurAmount), dtype=np.float32)
        k[ (blurAmount-1)// 2 , :] = np.ones(blurAmount, dtype=np.float32)
        k = cv2.warpAffine(k, cv2.getRotationMatrix2D(
            (blurAmount / 2 -0.5 , blurAmount / 2 -0.5 ) , angle, 1.0), (blurAmount, blurAmount) )  
        k = k * ( 1.0 / np.sum(k) )        

        return cv2.filter2D(img, -1, k) 
    

    originalsPath = f"Renders/{foldername}/Original"

    originalImagesNames = [f for f in os.listdir(originalsPath)\
            if os.path.isfile(os.path.join(originalsPath, f)) and f.endswith(".png")]

    originalImagesNames = sorted(originalImagesNames)

    # Make new folder for the results.

    blurredPath =  f"Renders/{foldername}/Post-processed"

    if not os.path.exists(blurredPath):     # Will this work on Windows? 
        os.makedirs(blurredPath)

    for imageName in originalImagesNames:
        
        blurAmount  =   random.randint(blurMin, blurMax)
        angle       =   random.randint(1, 360)

        img = cv2.imread(f"{originalsPath}/{imageName}", -1)    # The -1 preserves the bit depth.

        blurredImg = applyMotionBlur(img, blurAmount, angle)
        
        print(f"Blurrying {imageName} with strenght of {blurAmount} and {angle} degrees angle.")

        resultPath = f"{blurredPath}/{imageName.removesuffix('.png')}-blurred.png"
        cv2.imwrite(resultPath, blurredImg)

    if isnotebook():

        # Show the images in the post-processing folder in the notebook.

        for imageName in glob.glob(f'Renders/{foldername}/Post-processed/*.png'):
            display(Image(filename=imageName, width = 300, height = 300))


def sunglasses(camera, foldername):

    def wlToGap(wl, gaps):
        """
        Returns the equivalent airgap for a certain wavelength. For VIS!!!
        Wavelength in nm, please.
        """

        row = 0 # Is there a smarter solution for this?
        
        for item in gaps["WL1"]:

            if int(item) > wl: # int() truncates the value.

                gap = gaps.index[row - 1]
                break

            row += 1

        return gap


    def RGBThroughput(gap, integratedVIS):
        """
        Returns the R, G and B throughput (?) for a certain airgap, as a tuple.
        Gap in nm.
        """

        # Note: columns get renamed to | gap | gap.1 | gap.2 | when imported with Pandas.
        RThroughput = float(integratedVIS[str(gap)])
        GThroughput = float(integratedVIS[str(gap + .1)])
        BThroughput = float(integratedVIS[str(gap + .2)])

        return(RThroughput, GThroughput, BThroughput)


    def wlToQuantumEfficiencies(wl, quantumEff):
        """
        Returns Quantum efficiency for R, G, B for a given wavelength.
        """

        row = 0

        for item in quantumEff["WL[nm]"]:

            if int(item) > wl: # int() truncates the value.

                QERed = quantumEff.iloc[row - 1]["QE_Red"]
                QEBlue = quantumEff.iloc[row - 1]["QE_Blue"]
                QEGreen = quantumEff.iloc[row - 1]["QE_Green"]

                break

            row += 1
        
        # Reordering the output to RGB, I don't want any confusions.
        return (QERed, QEGreen, QEBlue)

    def filterAndMosaic(img, RGBThroughput, quantumEfficiencies):
        """
        ?
        """

        # Splitting into channels (CV2 uses BGR)
        (B, G, R) = cv2.split(img)

        # Channel "degradation"
        np.multiply(R, RGBThroughput[0], out=R, casting="unsafe")
        np.multiply(G, RGBThroughput[1], out=G, casting="unsafe")
        np.multiply(B, RGBThroughput[2], out=B, casting="unsafe")

        # Also with quantum efficiencies:
        np.multiply(R, quantumEfficiencies[0], out=R, casting="unsafe")
        np.multiply(G, quantumEfficiencies[1], out=G, casting="unsafe")
        np.multiply(B, quantumEfficiencies[2], out=B, casting="unsafe")


        # Pattern:
        # R G
        # G B
        # Future work: allow choice of pattern

        # Red
        # Keep odd rows and columns
        R_oddRows = np.delete(R, range(1, R.shape[0], 2), axis = 0)
        R_oddRows_oddColumns = np.delete(R_oddRows, range(1, R_oddRows.shape[1], 2), axis = 1)
        R_filtered = R_oddRows_oddColumns

        # Green 1
        # Keep odd rows and even columns
        G_oddRows = np.delete(G, range(1, G.shape[0], 2), axis = 0)
        G_oddRows_evenColumns = np.delete(G_oddRows, range(0, G_oddRows.shape[1], 2), axis = 1)
        G1_filtered = G_oddRows_evenColumns

        # Green 2
        # Keep even rows and odd columns
        G_evenRows = np.delete(G, range(0, G.shape[0], 2), axis = 0)
        G_evenRows_oddColumns = np.delete(G_evenRows, range(1, G_evenRows.shape[1], 2), axis = 1)
        G2_filtered = G_evenRows_oddColumns

        # Blue
        # Keep even rows and columns
        B_evenRows = np.delete(B, range(0, B.shape[0], 2), axis = 0) 
        B_evenRows_evenColumns = np.delete(B_evenRows, range(0, B_evenRows.shape[1], 2), axis = 1)
        B_filtered = B_evenRows_evenColumns

        return R, R_filtered, G, G1_filtered, G2_filtered, B, B_filtered  

    #######################################################

    if camera != "VIS" and camera != "NIR":

        print("Choose 'VIS' or 'NIR' as the camera")

        return

    # Folder containing sensor datafiles
    datafolder = "modules"  # Move them to modules/data at some point.

    # Folder with the images to be tested
    originalsPath = f"Renders/{foldername}/Original"
    filteredPath = f"Renders/{foldername}/Filtered"
    
    if not os.path.exists(filteredPath):     # Will this work on Windows? 
        os.makedirs(filteredPath)

    originalImagesNames = [f for f in os.listdir(originalsPath)\
            if os.path.isfile(os.path.join(originalsPath, f)) and f.endswith(".png")]

    originalImagesNames = sorted(originalImagesNames)

    if camera == "VIS":

        # Import throughput data
        vis = pd.read_csv(f"{datafolder}/vis_throughput.csv")

        # Column names are detected automatically, but row names are not.
        # The first column is called "0" in the original file. Could use
        # a better name, but I don't want to touch that.
        vis = vis.set_index("0")

        # Import gap_vs_wavelength dataset.

        gaps = pd.read_csv(f"{datafolder}/gap_vs_wavelength.csv")
        # Rename column names, as they contain spaces.
        gaps.columns = ["Gap", "WL8", "WL7", "WL6", "WL5", "WL4", "WL3", "WL2", "WL1"]
        # Using first column as row names
        gaps = gaps.set_index("Gap")

        # Integrate the values over columns in order to get the (?) total transmission.
        integratedVIS = vis.sum(axis = 0)
        integratedVIS = integratedVIS / 551   # (?) Normalization. Is it correct?

        # Import quantum efficiency file
        quantumEff = pd.read_csv(f"{datafolder}/quantum_eff_VIS.csv")
        # Using first column as row names
        #quantumEff = quantumEff.set_index("WL[nm]")


    multiplier = 2000  # Multiplier so you can distinguish something in the result.
    print(f"Multiplier: x{multiplier}")

    i = 1

    for imageName in originalImagesNames:

        print(f"Filtering image number {i}")

        # Extract wavelength
        #wavelength = float(p.match(imageName).group()[:-2])
        
        # Wavelengths are not saved to image names anymore...

        # Provisionally:
        wavelength = 500



        # Find correspoding gap
        gap = wlToGap(wavelength, gaps)
        # RGB throughput
        RGB = RGBThroughput(gap, integratedVIS)
     
        RGB = tuple(i * multiplier for i in RGB)

        # Quantum efficiency
        quantumEfficiencies = wlToQuantumEfficiencies(wavelength, quantumEff)
        #QERed, QEGreen, QEBlue = hf.wlToQuantumEfficiencies(wavelength, quantumEff)

        #img = cv2.imread(imageName, 1)
        img = cv2.imread(f"{originalsPath}/{imageName}", 1)
        #cv2.imwrite("Renders/Test/delet.png", img)
        
        print(RGB)
        print(quantumEfficiencies)

        
        # Throughput is multiplied in this function. Quantum efficiency should, too, right?
        # First: new function that (interpolates) looks for the values for the desired wavelength.
        # Next: multiply by the quantum effs, and correct the original file!!!!
        # Missing only the original file correction. Harder than it looks.

        R, R_filtered, G, G1_filtered, G2_filtered, B, B_filtered = \
                filterAndMosaic(img, RGB, quantumEfficiencies)

        # Saving result in new folder
        imageName = imageName.removesuffix(".png")

        # Red
        #cv2.imwrite(newFolderPath + "/" + imageName + "_filtered_R.png", R_filtered) 
        cv2.imwrite(f"{filteredPath}/{imageName}-filtered_R.png", R_filtered) 

        # Green1
        #cv2.imwrite(newFolderPath + "/" + imageName + "_filtered_G1.png", G1_filtered) 
        cv2.imwrite(f"{filteredPath}/{imageName}-filtered_G1.png", G1_filtered) 

        # Green2
        #cv2.imwrite(newFolderPath + "/" + imageName + "_filtered_G2.png", G2_filtered) 
        cv2.imwrite(f"{filteredPath}/{imageName}-filtered_G2.png", G2_filtered) 

        # Blue
        #cv2.imwrite(newFolderPath + "/" + imageName + "_filtered_B.png", B_filtered) 
        cv2.imwrite(f"{filteredPath}/{imageName}-filtered_B.png", B_filtered) 

        """
        # Grayscale!
        # This is the original image, right??
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        cv2.imwrite(newFolderPath + "/" + imageName + "_original.png", img) 
        #cv2.imwrite(newFolderPath + "/" + imageName + "_filtered_gray.png", img) 
        """

        i += 1