import configparser

class Settings:
    """
    Object that holds all settings, either read from a file
    or (TBD) all defaults.
    """

    def readFile(self, filepath):
        # Seems to not work!

        settings = configparser.ConfigParser()

        with open(filepath) as f:
            settings.read_file(f)

        # Paths

        pathSettings    =   settings["Paths"]   

        self.blenderPath    =   pathSettings["blenderPath"]
        self.dataPath       =   pathSettings["blenderDataPath"] # Need a better name for this.
        self.positionsPath  =   pathSettings["positionalDataFile"]
        self.outDir         =   pathSettings["outDir"]

        # Coordinates

        coordinatesSettings =   settings["Coordinates"]
        self.fromFile       =   coordinatesSettings.getboolean("readFromFile")
        self.angularCoords  =   coordinatesSettings.getboolean("angularCoordinates")



        self.sunCoordsCart     =   coordinatesSettings["cartesianSunCoordinates"]
        self.cameraCoordsCart  =   coordinatesSettings["cartesianCameraCoordinates"]

        self.sunCoordsAng      =   coordinatesSettings["angularSunCoordinates"]
        self.cameraCoordsAng   =   coordinatesSettings["angularCameraCoordinates"]


        # Transforming strings into lists of floats.

        self.sunCoordsCart      = [float(i.strip()) for i in self.sunCoordsCart[1:-1].split(",")]
        self.cameraCoordsCart   = [float(i.strip()) for i in self.cameraCoordsCart[1:-1].split(",")]

        self.sunCoordsAng      = [float(i.strip()) for i in self.sunCoordsAng[1:-1].split(",")]
        self.cameraCoordsAng   = [float(i.strip()) for i in self.cameraCoordsAng[1:-1].split(",")]



        # Material

        materialSettings    =   settings["Material"]

        self.surfaceFunction    =   materialSettings["surfaceFunction"]

        self.boulderFunction    =   materialSettings["boulderFunction"]

        self.imageAlbedo        =   materialSettings.getfloat("imageAlbedo")

        self.ROLOG0              =   materialSettings["ROLOG0"]
        self.ROLOG1              =   materialSettings["ROLOG1"]
        self.ROLOA0              =   materialSettings["ROLOA0"]
        self.ROLOA1              =   materialSettings["ROLOA1"]
        self.ROLOA2              =   materialSettings["ROLOA2"]
        self.ROLOA3              =   materialSettings["ROLOA3"]
        self.ROLOA4              =   materialSettings["ROLOA4"]

        self.McEEpsilon          =   materialSettings["McEEpsilon"]
        self.McEKsi              =   materialSettings["McEKsi"]
        self.McEEta              =   materialSettings["McEEta"]
        self.McEBeta             =   materialSettings["McEBeta"]
        self.McEGamma            =   materialSettings["McEGamma"]
        self.McEDelta            =   materialSettings["McEDelta"]

        # Target

        targetSettings  =   settings["Target"]

        self.target     =   targetSettings["target"]
        self.detail     =   targetSettings.getboolean("proceduralDetail")
        self.scale      =   targetSettings.getint("systemScale")
        self.rotation   =   targetSettings.getfloat("systemRotation")

        # Camera

        cameraSettings  =   settings["Camera"]

        self.camera     =   cameraSettings["camera"]
        self.resX       =   cameraSettings.getint("resolutionX")
        self.resY       =   cameraSettings.getint("resolutionY")
        self.fovX       =   cameraSettings.getfloat("fovX")
        self.fovY       =   cameraSettings.getfloat("fovY")

    @property   # Needed? This one is, the other is not!
                # It's because its overloading stuff?

    def defaults(self):
        # Seems to work!!
        self.blenderPath = ""

        # Paths

        self.blenderPath    =   ""  
        self.dataPath       =   ""
        self.positionsPath  =   ""
        self.outDir         =   ""

        # Coordinates

        self.fromFile       =   False
        self.angularCoords  =   False   # Needed?

        self.sunCoords      =   ["", "", ""]
        self.cameraCoords   =   ["", "", ""]

        # Material

        self.surfaceFunction    =   ""
        self.boulderFunction    =   ""

        self.imageAlbedo        =   0

        self.ROLOG0             =   0
        self.ROLOG1             =   0
        self.ROLOA0             =   0
        self.ROLOA1             =   0
        self.ROLOA2             =   0
        self.ROLOA3             =   0
        self.ROLOA4             =   0

        self.McEEpsilon         =   0
        self.McEKsi             =   0
        #self.McENu              =   0
        self.McEEta              =   0
        self.McEBeta            =   0
        self.McEGamma           =   0
        self.McEDelta           =   0

        # Target

        self.target     =   ""
        self.detail     =   False
        self.scale      =   1
        self.rotation   =   0

        # Camera

        self.camera     =   ""
        self.resX       =   0
        self.resY       =   0
        self.fovX       =   0
        self.fovY       =   0
