[Paths]	##############################################################################

# Path of Blender executable (Use version 3+)

#blenderPath         =   C:/HYapp/Blender 3.0/blender.exe
blenderPath			=	/home/mario/Programs/blender-3/./blender

# Path to package root folder.
# Specify absolute path if running outside the package folder.
# Leave empty if running from the package root.
blenderDataPath		= 	
#blenderDataPath			=		C:/HYapp/PSR-Programs/GitHub/asteroid-image-generator

# Path of the positional datafile, can be empty if readFromFile = False

positionalDatafile  =   data/positionalDataShort.txt
#positionalDatafile  =   data/positionalDataAngular.txt


# Path for storing the results (leave empty for default foldername convention)

outDir				=	test
#outdir				=	


[Coordinates]	######################################################################


# If True, coordinates will be read from positionalDataFile.
# If False, the values down below will be used.
# (Or if empty?)

readFromFile		=	True

# Use of angular coordinates; cartesian are expected if False.

angularCoordinates	=	False

# Cartesian or Angular will be used depending on angularCoordinates value.

# All cartesian values in km
cartesianSunCoordinates		=   [1.505, 1.314, -2.396]
cartesianCameraCoordinates	=   [6.973, 5.626, -3.465]

# [lat (°), lon (°)]
angularSunCoordinates		=   [30, 60]
# [lat (°), lon (°), distance (km)]
angularCameraCoordinates	=   [0, 180, 10]    



[Material]	########################################################################

# Choice of photometric function.
# Options: 1: Lommel-Seeliger, 2: ROLO, 3: McEwen, 4: Lambert 

surfaceFunction		=	ROLO

boulderFunction		=   ROLO

# Parameters for ROLO, current values (pan. filter) from Golish et al. (2021), Icarus 357.
# However, we set A3 and A4 to zero by default since their estiamted values are so small
# that they don't infuence the model behavior anyway
ROLOG0		=		0.010
ROLOG1		=		0.2729
ROLOA0		=		0.07936
ROLOA1		=		-0.002191
ROLOA2		=		0.000037
ROLOA3		=		0.0
ROLOA4		=		0.0

# Parameters for McEwen, current values (pan. filter) from Golish et al. (2021), Icarus 357.
# However, we set gamma and delta to zero by default since their estiamted values are so small
# that they don't infuence the model behavior anyway
McEEpsilon          =   -0.009
McEKsi              =   0.0
#McENu               =   0.0
McEEta               =   0.0
McEBeta             =		-0.03305
McEGamma            =   0.0
McEDelta            =   0.0

# Image albedo.
# This controls how the brightness scales to the 16-bit integer scale of the
# RGB images. The value of image albedo is the RGB value (in [0,1] range) of
# a surface element when both illuminated and viewed from zenith. For 
# Lommel-Seeliger family photometric functions (1-3) the surface brightness
# grows with viewing angle approaching zero, therefore image albedo value of
# 0.5 at maximum is safe for RGB values not going over the (0,1) scale.
# For maximum dynamic range in images, high values up to 0.5 are recommended. For
# 'realistically dark' images one can try smaller values. The value of 
# imageAlbedo is saved in the 'settings.txt' file with the produced image(s).
# This can be used later for absolute calibration.
imageAlbedo		=	0.4

[Target]	#########################################################################

# Target body selection.
# Options:  1: DRAv3.03 (Didymos & Dimorphos)       2: BISv1.0 (Bennu & Itokawa, scaled)
#           3: RISv1.0  (Ryugu & Itokawa, scaled)   4: Sphere
#           5: 67P (Churyumov-Gerasimenko)          6: Test asteroid Blender 3.0

target				=	RISv1.0


# Use of procedural detail on top of the model.

proceduralDetail		=	True	


# Scale multiplier for the model (unitless)

systemScale			=	1

# System rotation (in degrees). Used only if readFromFile is False

systemRotation		=	180



[Camera]	#########################################################################

# Camera selection
# Options: ASPECT-VIS, ASPECT-NIR, ASPECT-VIS-Green, or leave empty??

#camera              =   ASPECT-NIR
camera              =

# If camera == False, the following settings will be used:
# If camera not set, the following settings will be used:

# Resolutions in pixels
resolutionX         =   512
resolutionY         =   512

# Fields of view in radians
fovX                =   .17
fovY                =   .17
