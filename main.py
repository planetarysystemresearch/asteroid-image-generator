"""
Copyright 2021 Mario Palos, Antti Penttilä, Tomas Kohout (Institute of Geology of the Czech Academy of Sciences and University of Helsinki)
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import os
import sys
import argparse
import subprocess

from modules.settings_parsing import Settings

#----------------------------------------------------------------------------------#

# ARGUMENT PARSING
# Processes the user options on the command line.

argv = sys.argv

parser = argparse.ArgumentParser()

parser.add_argument("-s", "--skipConfirmation", dest = "skipConfirmation", action = "store_true",
        required = False, help = "Skips the user confirmation step.")

parser.add_argument("-p", "--justPrint", dest = "justPrint", action = "store_true",
        required = False, help = "Do not run Blender, just print the command.")

parser.add_argument("-c", "--customSettingsPath", dest = "customSettingsPath",
        required = False, help = "Path to  a custom settings file, relative to project folder.")

args = parser.parse_args()

#----------------------------------------------------------------------------------#

# SETTINGS FILE PARSING


settings = Settings()

if args.customSettingsPath:

    settings.readFile(args.customSettingsPath)

else:

    settings.readFile("settings.txt")


#----------------------------------------------------------------------------------#

# Confirmation menu

if not args.justPrint:

    if args.customSettingsPath:
        print(f"\nCurrent settings (read from {args.customSettingsPath}):\n")
    else:
        print("\nCurrent settings (read from settings.py):\n")

    ## Paths

    print(f"* Blender path: {settings.blenderPath}")

    print("")

    ## File / User input

    if settings.fromFile:
        print(f"* Reading positional data from {settings.positionsPath}.")
    else:
        print("* Using user-defined coordinates.")

        #print(f"** Setting the sun at {str(settings.sunCoords)} and"
        #      f"the camera at {str(settings.cameraCoords)}.")

        if settings.angularCoords:

            sunCoords       = settings.sunCoordsAng
            cameraCoords    = settings.cameraCoordsAng

        else:

            sunCoords       = settings.sunCoordsCart
            cameraCoords    = settings.cameraCoordsCart  

        print(f"** Setting the sun at {str(sunCoords)} and"
              f"the camera at {str(cameraCoords)}.")

    if settings.outDir != "":
        print(f"* Output will be saved to Renders/{settings.outDir}")


    print(f"* Surface photometric function selected: {settings.surfaceFunction}")

    print(f"* Boulder photometric function selected: {settings.boulderFunction}")

    ## Target name

    print(f"* Selected target: {settings.target}.")

    ## Procedural detail

    print(f"* Procedural detail state: {str(settings.detail)}.")

    ## System rotation

    if not settings.fromFile:
        print(f"* System rotation: {str(settings.rotation)} degrees.")

    ## System scale

    print(f"* System scale: {settings.scale}.")


    print(f"* Image albedo: {settings.imageAlbedo}")

    ## Camera parameters

    print("")
    print("Camera settings:")
    print("")

    if settings.camera != "":
        print(f"* Camera in use: {settings.camera}.")
    else:
        print(f"* Resolution: {settings.resX} x {settings.resY} pixels.")
        print(f"* Field of view: {settings.fovX} x {settings.fovY} rad.")

    print("")

if args.skipConfirmation or args.justPrint:

    confirmation = "y"

else:

    confirmation = input ("Do you want to proceed? (y/n): ")

#----------------------------------------------------------------------------------#

# Blender call

if confirmation.lower() == "y":
    # Runs blenderControl.py with the chosen parameters.
    # With this approach, everything has to be sent as strings, even vectors.
    # They are reinterpreted back in blenderControl.
    
    # Path for Blender files if running outside the package root folder.
    if settings.dataPath != "":
        dataPath = settings.dataPath
    else:
        dataPath = "."
    
    # Basic arguments that will always be passed on:
    mainArgs = [
        settings.blenderPath,                        # Blender executable path.
        "--background",                     # Run Blender in the background.
        os.path.join(dataPath,
            "asteroidImageGenerator.blend"),    # Blender file to be run.
        "--python",                         # Execute a python script with the Blender file.
        os.path.join(dataPath,
            "blenderControl.py"),                # Python script file to be run.
    ]

    # Custom options:

    # This double dash marks the start of the custom options:
    options = ["--"]

    ## Common options


    # Experimental: outDir

    if settings.outDir:
        options += ["--outDir", settings.outDir]

    if settings.angularCoords:
        options += ["--angularCoordinates"]

    if settings.fromFile:
        options += ["--fromFile"]

        options += ["--positionalData", settings.positionsPath]

    else:
        #options += ["--coordinates", str(settings.sunCoords), str(settings.cameraCoords)]
        options += ["--coordinates", str(sunCoords), str(cameraCoords)]

    options += ["--imageAlbedo", str(settings.imageAlbedo)]
    
    options += ["--surfaceFunctionName", settings.surfaceFunction]
    options += ["--boulderFunctionName", settings.boulderFunction]

    if settings.surfaceFunction == "ROLO" or settings.boulderFunction == "ROLO":

        options += ["--ROLOG0", str(settings.ROLOG0)]
        options += ["--ROLOG1", str(settings.ROLOG1)]
        options += ["--ROLOA0", str(settings.ROLOA0)]
        options += ["--ROLOA1", str(settings.ROLOA1)]
        options += ["--ROLOA2", str(settings.ROLOA2)]
        options += ["--ROLOA3", str(settings.ROLOA3)]
        options += ["--ROLOA4", str(settings.ROLOA4)]

    if settings.surfaceFunction == "McEwen" or settings.boulderFunction == "McEwen":

        options += ["--McEEpsilon", str(settings.McEEpsilon)]
        options += ["--McEKsi", str(settings.McEKsi)]
        #options += ["--McENu", str(settings.McENu)]
        options += ["--McEEta", str(settings.McEEta)]
        options += ["--McEBeta", str(settings.McEBeta)]
        options += ["--McEGamma", str(settings.McEGamma)]
        options += ["--McEDelta", str(settings.McEDelta)]

    options += ["--target", settings.target]
    
    options += ["--proceduralDetail", str(settings.detail)]
    options += ["--systemRotation", str(settings.rotation)]
    options += ["--systemScale", str(settings.scale)]

    ## Camera specific options


    if settings.camera == "ASPECT-VIS":

        options += [
                "--resolution", "1024", "1024",
                "--fov", "0.174533", "0.174533",    # 10 degrees in radians
                ]

    elif settings.camera == "ASPECT-NIR":

        options += [
                "--resolution", "640", "512",       # pixels
                "--fov", "0.116937", "0.0942478",   # 6.7 x 5.4 degrees in radians
                ]

    elif settings.camera == "ASPECT-VIS-Green":

        options += [
                "--resolution", "1024", "1024",     # pixels
                "--fov", "0.174533", "0.174533",    # 10 degrees in radians
                ]

    else:
        """
        Default options for when no real camera is used.
        """

        # Render resolution
        options += ["--resolution", str(settings.resX), str(settings.resY)]

        # Field of view
        options += ["--fov", str(settings.fovX), str(settings.fovY)]


    if args.justPrint:
        print(' '.join(mainArgs + options))

    else:
        subprocess.run(mainArgs + options)
else:

    print("Closing")
